﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackLogic : MonoBehaviour
{
    [SerializeField] private Transform m_PlayerCamera = null;
    [SerializeField] private int m_AttackDamage = 50;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            FireWeapon();
        }
    }

    private void FireWeapon()
    {
        Debug.Log("Firingweapon");
        RaycastHit m_RaycastHit;
        int layerMask = 1 << 8;

        if (Physics.Raycast(m_PlayerCamera.position, m_PlayerCamera.TransformDirection(Vector3.forward), out m_RaycastHit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(m_PlayerCamera.position, m_PlayerCamera.TransformDirection(Vector3.forward) * m_RaycastHit.distance, Color.red);
            m_RaycastHit.transform.gameObject.GetComponent<EnemyScript>().ReceiveDamage(m_AttackDamage);
        }
        else
        {
            Debug.DrawRay(m_PlayerCamera.position, m_PlayerCamera.TransformDirection(Vector3.forward) * 1000, Color.yellow);
        }
    }
}
