﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{
    private enum PlayerState
    {
        Stand,
        Move,
        AirMove,
    }

    [Header("Movement Parameters")]
    [SerializeField] public float movementSpeed = 15f;
    [SerializeField] private float playerGravity = -9f;

    [Header("Camera Parameters")]
    [SerializeField] private float lowCameraClampAngle = -90f;
    [SerializeField] private float highCameraClampAngle = 90f;
    [SerializeField] public float cameraSensitivityX = 120f;
    [SerializeField] public float cameraSensitivityY = 120f;
    [SerializeField] public bool invertCameraX = false;
    [SerializeField] public bool invertCameraY = false;
    private float cameraXRotation = 0f;

    [Header("Debug Info")]
    [SerializeField] public bool PlayerActive = true;
    [SerializeField] private PlayerState currentPlayerState;
    [SerializeField] private Vector3 movementInput;
    [SerializeField] private Vector2 cameraInput;

    private GameObject playerCamera = null;
    private CharacterController localCharacterController = null;

    private static FPSController instance;
    public static FPSController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<FPSController>();
            }
            return instance;
        }
    }

    void Awake()
    {
        playerCamera = GetComponentInChildren<Camera>().gameObject;
        localCharacterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        GetPlayerInput();

        if (PlayerActive)
        {
            CheckCharacterState();
            RotatePlayer();
        }

        if (!localCharacterController.isGrounded)
        {
            ApplyGravity();
        }
    }

    //Checks the current PlayerState and performs the appropriate actions.
    private void CheckCharacterState()
    {
        if (currentPlayerState == PlayerState.Move)
        {
            MovePlayerGround();
        }
    }

    //Moves the player along the ground.
    private void MovePlayerGround()
    {
        localCharacterController.Move(movementInput * movementSpeed * Time.deltaTime);
    }

    //Rotates the player body and camera.
    private void RotatePlayer()
    {
        //Rotate player body.
        transform.Rotate(Vector3.up * cameraInput.x);

        //Rotate camera.
        cameraXRotation -= cameraInput.y;
        cameraXRotation = Mathf.Clamp(cameraXRotation, lowCameraClampAngle, highCameraClampAngle);
        playerCamera.transform.localRotation = Quaternion.Euler(cameraXRotation, 0f, 0f);
    }

    //Checks for input from the player.
    private void GetPlayerInput()
    {
        //Get player movement from Horiztonal and Vertical axes. Set player to playerState.Move if appropriate.
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            movementInput = transform.right * x + transform.forward * z;
            currentPlayerState = PlayerState.Move;
        }
        else
        {
            currentPlayerState = PlayerState.Stand;
        }

        //Get camera rotation from MouseX and MouseY axes.
        cameraInput.x = Input.GetAxis("Mouse X") * cameraSensitivityX * Time.deltaTime;
        cameraInput.y = Input.GetAxis("Mouse Y")* cameraSensitivityY * Time.deltaTime;

        if (invertCameraX)
        {
            cameraInput.x = -cameraInput.x;
        }

        if (invertCameraY)
        {
            cameraInput.y = -cameraInput.y;
        }
    }

    private void ApplyGravity()
    {
        localCharacterController.Move(new Vector3(0, playerGravity, 0));
    }
}