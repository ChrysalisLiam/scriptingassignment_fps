﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerLogic : MonoBehaviour
{
    [SerializeField] private float m_pointDegradationInterval = 0.5f;
    [SerializeField] public int m_currentlyCarriedPoints = 0;
    [SerializeField] private int m_currentDegradationMultiplier = 0;

    [SerializeField] private TMP_Text m_carriedPointTicker = null;
    private int m_payloadDisplayLength = 6;

    private float m_pointDegradationTimer = 0;
    private string m_adjustedScoreString = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PointObject")
        {
            //Take points from the point object, then destroy that point object.
            m_pointDegradationTimer = m_pointDegradationInterval;
            m_currentDegradationMultiplier++;
            m_currentlyCarriedPoints += other.gameObject.GetComponent<PointObject>().m_scoreValue;
            RefreshCarriedPointTickerText();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "DeliveryZone")
        {
            //Remove all player points and submit them to the Game Manager.
            GameManager.Instance.UpdateScoreTicker(m_currentlyCarriedPoints);
            m_currentlyCarriedPoints = 0;
            m_currentDegradationMultiplier = 0;
            RefreshCarriedPointTickerText();
        }
    }

    void Update()
    {
        //if the player is carrying points, they will degrade over time.
        if (m_currentlyCarriedPoints > 0)
        {
            DegradeCarriedPoints();
        }
    }

    private void DegradeCarriedPoints()
    {
        m_pointDegradationTimer -= Time.deltaTime;

        if (m_pointDegradationTimer <=0)
        {
            //Subtract one point from currently carried points and reset timer.
            m_currentlyCarriedPoints -= m_currentDegradationMultiplier;
            m_pointDegradationTimer = m_pointDegradationInterval;
            RefreshCarriedPointTickerText();
        }
    }

    //Updates the PAYLOAD UI element with a six digit number.
    private void RefreshCarriedPointTickerText()
    {
        m_adjustedScoreString = null;

        string m_payloadPoints = m_currentlyCarriedPoints.ToString();
        int m_additionalZeroes = m_payloadDisplayLength - m_payloadPoints.Length;

        for (int i = 0; i < m_additionalZeroes; i++)
        {
            m_adjustedScoreString += "0";
        }

        m_adjustedScoreString += m_payloadPoints;

        m_carriedPointTicker.text = ("PAYLOAD " + m_adjustedScoreString);
    }
}
