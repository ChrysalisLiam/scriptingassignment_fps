﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class TitleScreenButton : MonoBehaviour
{
    [SerializeField] private string m_SceneToLoad = null;
    [SerializeField] private GameObject m_FadeObject = null;

    private bool m_buttonClicked = false;
    private Button m_LocalButtonComponent = null;
    private Color m_PressedColor;
    private Color m_NormalColor;

    private void Awake()
    {
        m_LocalButtonComponent = GetComponent<Button>();
        m_PressedColor = m_LocalButtonComponent.colors.pressedColor;
        m_NormalColor = m_LocalButtonComponent.colors.normalColor;
        m_FadeObject.SetActive(false);
    }

    public void OnClickStartGame()
    {
        if (m_buttonClicked == false)
        {
            WobbleButton();
            Invoke("StartGame", 2);
            m_buttonClicked = true;
            FadeAssignedObject();
        }
    }
    private void StartGame()
    {
        SceneManager.LoadScene(m_SceneToLoad);
    }

    public void OnClickHowToPlay()
    {
        WobbleButton();
        m_FadeObject.SetActive(true);
    }

    private void WobbleButton()
    {
        transform.DOPunchScale(new Vector3(1, 1, 0), 1.5f, 10, 0.4f);
    }

    private void FadeAssignedObject()
    {
        m_FadeObject.SetActive(true);
        m_FadeObject.GetComponent<ImageColorFader>().StartFade();
    }
}