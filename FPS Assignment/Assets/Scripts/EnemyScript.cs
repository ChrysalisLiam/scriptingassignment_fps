﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] private int m_MaxLife = 100;
    [SerializeField] private GameObject m_DropItem = null;
    private int m_currentLife;

    private void Awake()
    {
        m_currentLife = m_MaxLife;
    }

    public void ReceiveDamage(int incomingDamage)
    {
        m_currentLife -= incomingDamage;

        if (m_currentLife <= 0)
        {
            Die();
        }
    }

    public void Die()
    {   
        if (m_DropItem != null)
        {
            Instantiate(m_DropItem, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
