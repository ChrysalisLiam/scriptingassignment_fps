﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ImageColorFader : MonoBehaviour
{
    [SerializeField] private Color m_OriginalColor;
    [SerializeField] private Color m_TargetFadeColor = Color.clear;
    [SerializeField] private float m_ColorFadeSpeed = 1.9f;

    private Image m_localUIImage = null;
    private bool m_CurrentColorState = true;
    //true = Currently colored m_OriginalColor.
    //false = Currently colored m_TargetFadeColor.

    void Awake()
    {
        m_localUIImage = GetComponent<Image>();
        m_OriginalColor = m_localUIImage.color;
    }

    public void StartFade()
    {
        if (m_CurrentColorState)
        {
            m_localUIImage.DOColor(m_TargetFadeColor, m_ColorFadeSpeed);
            m_CurrentColorState = false;
        }
        else if (!m_CurrentColorState)
        {
            m_localUIImage.DOColor(m_OriginalColor, m_ColorFadeSpeed);
            m_CurrentColorState = true;
        }
    }
}
