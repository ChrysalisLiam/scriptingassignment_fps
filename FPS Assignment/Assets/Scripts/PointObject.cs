﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointObject : MonoBehaviour
{
    [SerializeField] public int m_scoreValue = 300;
    [SerializeField] public float m_StartForce;
    [SerializeField] public Vector2 m_RandomForceLimits;

    private Rigidbody m_localRB = null;

    private void Awake()
    {
        m_localRB = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        m_localRB.AddForce(Random.Range(m_RandomForceLimits.x,m_RandomForceLimits.y), m_StartForce, Random.Range(m_RandomForceLimits.x, m_RandomForceLimits.y));
    }
}
