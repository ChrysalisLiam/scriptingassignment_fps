﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Game Parameters")]
    [SerializeField] private int m_StartingTime = 125;
    public bool m_TimerActive = true;

    [Header("Gameplay UI Elements")]
    [SerializeField] private TMP_Text m_ScoreTicker = null;
    [SerializeField] private TMP_Text m_TimeTicker = null;

    [Header("Endgame Elements")]
    [SerializeField] private string m_SceneToLoad = null;
    [SerializeField] private GameObject m_GameplayUIGroup = null;
    [SerializeField] private GameObject m_FinalScoreText = null;
    [SerializeField] private GameObject m_CongratulationsText = null;
    [SerializeField] private GameObject m_FinalScoreTicker = null;
    [SerializeField] private ImageColorFader m_WhiteScreenFilter = null;

    private string m_AdjustedScoreString = null;
    private int m_CurrentScore = 0;
    private int m_ScoreDisplayLength = 6;

    private float m_TimeLeft = 0;
    private string m_MinutesLeft = null;
    private string m_SecondsLeft = null;

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }

    void Awake()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        //Initialize timer and score.
        m_CurrentScore = 0;
        m_TimeLeft = m_StartingTime;
        UpdateTimeTicker();
        UpdateScoreTicker(0);
    }

    private void Update()
    {
        if (m_TimerActive)
        {
            m_TimeLeft -= 1 * Time.deltaTime;
            UpdateTimeTicker();
        }
    }

    //Updates the TIME UI element with a minute and second string (eg.: 02:45).
    public void UpdateTimeTicker()
    {
        if (m_TimeLeft <= 0)
        {
            EndGame();
        }
        else
        {
            m_MinutesLeft = Mathf.FloorToInt(m_TimeLeft / 60).ToString("00");
            m_SecondsLeft = Mathf.FloorToInt(m_TimeLeft % 60).ToString("00");
            m_TimeTicker.text = ("TIME " + m_MinutesLeft + ":" + m_SecondsLeft);
        }
    }

    //Updates the SCORE UI element with a six digit number.
    public void UpdateScoreTicker(int additionalPoints)
    {
        m_CurrentScore += additionalPoints;
        m_AdjustedScoreString = null;

        string m_currentScoreString = m_CurrentScore.ToString();
        int m_additionalZeroes = m_ScoreDisplayLength - m_currentScoreString.Length;

        for (int i = 0; i < m_additionalZeroes; i++)
        {
            m_AdjustedScoreString += "0";
        }

        m_AdjustedScoreString += m_currentScoreString;

        m_ScoreTicker.text = ("SCORE " + m_AdjustedScoreString);
    }

    private void EndGame()
    {
        m_TimerActive = false;
        FPSController.Instance.PlayerActive = false;

        m_GameplayUIGroup.SetActive(false);
        m_FinalScoreText.SetActive(true);
        m_CongratulationsText.SetActive(true);
        m_WhiteScreenFilter.StartFade();
        m_FinalScoreTicker.gameObject.SetActive(true);
        m_FinalScoreTicker.GetComponent<TMP_Text>().text = m_CurrentScore.ToString();
        Invoke("ReturnToMenu", 4f);
    }

    private void ReturnToMenu()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(m_SceneToLoad);
    }
}
